package test

import (
	"testing"

	"gitlab.com/nikn13/jenkinsonartest/src/mul"
)

func TestMul(t *testing.T) {
	_, err := mul.Mul(4, 6)
	if err != nil {
		t.Error(err.Error())
	}
}

func TestMulInvalid(t *testing.T) {
	_, err := mul.Mul(4.5, 6)
	if err == nil {
		t.Error("Invalid case failed")
	}
}
