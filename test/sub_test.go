package test

import (
	"testing"

	"gitlab.com/nikn13/jenkinsonartest/src/sub"
)

func TestSub(t *testing.T) {
	_, err := sub.Sub(4, 6)
	if err != nil {
		t.Error(err.Error())
	}
}

func TestSubInvalid(t *testing.T) {
	_, err := sub.Sub(4.5, 6)
	if err == nil {
		t.Error("Invalid case failed")
	}
}
