package test

import (
	"testing"

	"gitlab.com/nikn13/jenkinsonartest/src/add"
)

func TestAdd(t *testing.T) {
	_, err := add.Add(4, 6)
	if err != nil {
		t.Error(err.Error())
	}
}

func TestAddInvalid(t *testing.T) {
	_, err := add.Add(4.5, 6)
	if err == nil {
		t.Error("Invalid case failed")
	}
}
