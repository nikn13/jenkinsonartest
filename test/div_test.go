package test

import (
	"testing"

	"gitlab.com/nikn13/jenkinsonartest/src/div"
)

func TestDiv(t *testing.T) {
	_, err := div.Div(4, 6)
	if err != nil {
		t.Error(err.Error())
	}
}

func TestDivInvalid(t *testing.T) {
	_, err := div.Div(4.5, 6)
	if err == nil {
		t.Error("Invalid case failed")
	}
}

func TestDivZeroCase(t *testing.T) {
	_, err := div.Div(4, 0)
	if err == nil {
		t.Error("Invalid case failed divide by zero")
	}
}
