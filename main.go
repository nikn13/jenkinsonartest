package main

import (
	"fmt"

	"gitlab.com/nikn13/jenkinsonartest/src/add"
	"gitlab.com/nikn13/jenkinsonartest/src/mul"
	"gitlab.com/nikn13/jenkinsonartest/src/sub"
)

func main() {
	sum, err := add.Add(4, 5)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(*sum)

	sum, err = sub.Sub(4, 5)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(*sum)

	sum, err = mul.Mul(4, 5)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(*sum)

}
