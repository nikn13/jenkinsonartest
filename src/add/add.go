package add

import (
	"errors"
	"reflect"
)

func Add(a, b interface{}) (*int, error) {
	xt := reflect.TypeOf(a).Kind()
	xy := reflect.TypeOf(b).Kind()
	if xt != reflect.Int || xy != reflect.Int {
		return nil, errors.New("invalid data type please pass integer")
	}
	sum := a.(int) + b.(int)
	return &sum, nil
}
