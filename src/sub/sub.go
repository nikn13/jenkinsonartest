package sub

import (
	"errors"
	"reflect"
)

func Sub(a, b interface{}) (*int, error) {
	xt := reflect.TypeOf(a).Kind()
	xy := reflect.TypeOf(b).Kind()
	if xt != reflect.Int || xy != reflect.Int {
		return nil, errors.New("invalid data type please pass integer")
	}
	diff := a.(int) - b.(int)
	return &diff, nil
}
